function [RMSE, mPerf, varargout] = ComputePerformance(estimation,...
    correct, Type, Method, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Compute prediction performance and error
%--------------------------------------------------------------------------
% Takes as arguments:
%   - estimation -> network output ( nSymbols x T )
%   - correct -> target output ( nSymbols x T )
%   - Type -> nature of the task (FSG/Elman/Counting/AnBn/AnBnCn)
%   - Method:
%       o Exact = determined the network's ability to exactly predict the 
%           next symbol in the sequence
%       o Theoretical = estimation is taken as a next-symbol probability
%           distribution and compared to a target distribution (this is
%           dependent on the task specifications...)
%       o Train = computes target distribution based on the statistics 
%           of the training data
%==========================================================================
%--------------------------------------------------------------------------
o_min = varargin{1};
seq   = varargin{2};


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(Type, 'FSG') 
    %%%-----------------------------------------------------------------%%%
    if strcmp(Method, 'Exact')
        % Winner-takes-all:
        [~, idx_est] = max(estimation, [], 1);
        [~, idx_cor] = max(correct, [], 1);
        
        % mean performance:
        prdiff = (idx_est==idx_cor); mPerf = mean(prdiff);
        
        % root mean squared error:
        sqErr = (correct-estimation).^2;
        MSE   = mean(sqErr);
        RMSE  = sqrt(MSE);
        
        %
        varargout{1} = MSE;
    %%%-----------------------------------------------------------------%%%    
    elseif strcmp(Method, 'Theoretical')
        % Interpret the output as a prob distribution for the next symbol:
        estimation(estimation <= o_min) = o_min;
        estimation = normCols(estimation);

        % Mean Performance computed as the average normalized KL divergence
        % between estimated probabilities and target
        [mPerf, KL, Div] = computeKLPerf(estimation, correct);
        
        % Performance evaluated by the normalized negative log likelihood
        NNL = computeNNLperf(seq, correct, estimation);
        
        % Performance evaluated by the cosine score:
        COS = computeCOSperf(correct, estimation, seq);
        
        % root mean squared error:
        sqErr = (correct-estimation).^2;
        MSE   = mean(sqErr);
        RMSE  = sqrt(MSE);

        %
        varargout{1} = KL;
        varargout{2} = Div;
        varargout{3} = MSE;
        varargout{4} = NNL;
        varargout{5} = COS;
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'Counting')
     %%%-----------------------------------------------------------------%%%
    if strcmp(Method, 'Exact')
        % Winner-takes-all:
        [~, idx_est] = max(estimation, [], 1);
        [~, idx_cor] = max(correct, [], 1);
        
        % mean performance:
        prdiff = (idx_est==idx_cor); mPerf = mean(prdiff);
        
        % root mean squared error:
        sqErr = (correct-estimation).^2;
        MSE   = mean(sqErr);
        RMSE  = sqrt(MSE);
        
        %
        varargout{1} = MSE;
    %%%-----------------------------------------------------------------%%%    
    elseif strcmp(Method, 'Theoretical')
        % Interpret the output as a prob distribution for the next symbol:
        estimation(estimation <= o_min) = o_min;
        estimation = normCols(estimation);
        
        % compute performance only in the "extremes" of the word (a, e, c,
        % f) - a and e can occur with equal probability, whereas c and f
        % are deterministic
        
        % consider only the output that predicts these symbols:
        
        sym = {'a','c','e','f'};
 
        target = [[0.5; 0; 0; 0; 0.5; 0], [0; 0; 1; 0; 0; 0], ...
            [0.5; 0; 0; 0; 0.5; 0], [0; 0; 0; 0; 0; 1]];
        
        for i = 1:4
            idx = strfind(seq, sym{i});
            
            for n = 1:length(idx)
                if idx(n) > 1
                    newOut(:,n) = estimation(:,idx(n)-1);
                    targ(:,n)   = target(:, i);
                end
            end
            NO{i} = newOut;
            Nt{i} = targ;
        end
        
        estim = [NO{:}];
        tget  = [Nt{:}];
        
        [mPerf, KL, Div] = computeKLPerf(estim, tget);
        mPerf = real(mPerf);
        
        % root mean squared error:
        sqErr = (tget-estim).^2;
        MSE   = mean(sqErr);
        RMSE  = sqrt(MSE);

        
        varargout{1} = KL;
        varargout{2} = Div;
        varargout{3} = MSE;
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'Elman')
     %%%-----------------------------------------------------------------%%%
     if strcmp(Method, 'Exact')
        % Winner-takes-all:
        [~, idx_est] = max(estimation, [], 1);
        [~, idx_cor] = max(correct, [], 1);
        
        % mean performance:
        prdiff = (idx_est==idx_cor); mPerf = mean(prdiff);
        
        % root mean squared error:
        sqErr = (correct-estimation).^2;
        MSE   = mean(sqErr);
        RMSE  = sqrt(MSE);
        
        %
        varargout{1} = MSE;
        
     %%%-----------------------------------------------------------------%%%    
     elseif strcmp(Method, 'Theoretical')
        % Interpret the output as a prob distribution for the next symbol:
        estimation(estimation <= o_min) = o_min;
        estimation = normCols(estimation);
         
        % correct for the prediction of only the deterministic part, i.e.,
        % the occurences of 'a', 'ii' and 'uuu'
        
        % consider only the output that predicts these symbols:
        
        sym = {'a','i','u'};
 
        target = [[1; 0; 0; 0; 0; 0], [0; 0; 0; 0; 1; 0], ...
            [0; 0; 0; 0; 0; 1]];
        
        for i = 1:3
            idx = strfind(seq, sym{i});
            
            for n = 1:length(idx)
                if idx(n) > 1
                    newOut(:,n) = estimation(:,idx(n)-1);
                    targ(:,n)   = target(:, i);
                end
            end
            NO{i} = newOut;
            Nt{i} = targ;
        end
        
        estim = [NO{:}];
        tget  = [Nt{:}];
        
        [mPerf, KL, Div] = computeKLPerf(estim, tget);
        mPerf = real(mPerf);
        
        % root mean squared error:
        sqErr = (tget-estim).^2;
        MSE   = mean(sqErr);
        RMSE  = sqrt(MSE);

        
        varargout{1} = KL;
        varargout{2} = Div;
        varargout{3} = MSE;
     end
    

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(Type, 'AnBnCn')
    %%%-----------------------------------------------------------------%%%
    if strcmp(Method, 'Exact')
        idB = strfind(seq, 'B');
        idC = strfind(seq, 'C');
        
        ID = [idB, idC]; ID = sort(ID);
        
        for i = 1:length(ID)
            
            if ID(i) <= length(estimation)
                
                NewEst(:,i) = estimation(:,ID(i));
                NewCor(:,i) = correct(:, ID(i));
            end
        end
        
        
        % Winner-takes-all:
        [~, idx_est] = max(NewEst, [], 1);
        [~, idx_cor] = max(NewCor, [], 1);
        
        % mean performance:
        prdiff = (idx_est==idx_cor); mPerf = mean(prdiff);
        
        % root mean squared error:
        sqErr = (correct-estimation).^2;
        MSE   = mean(sqErr);
        RMSE  = sqrt(MSE);
        
        %
        varargout{1} = MSE;
        
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'AnBn')
    %%%-----------------------------------------------------------------%%%
    if strcmp(Method, 'Exact')
       idB = strfind(seq, 'B');
       ID = sort(idB);
        
        for i = 1:length(ID)
            
            if ID(i) <= length(estimation)
                
                NewEst(:,i) = estimation(:,ID(i));
                NewCor(:,i) = correct(:, ID(i));
            end
        end
        
        
        % Winner-takes-all:
        [~, idx_est] = max(NewEst, [], 1);
        [~, idx_cor] = max(NewCor, [], 1);
        
        % mean performance:
        prdiff = (idx_est==idx_cor); mPerf = mean(prdiff);
        
        % root mean squared error:
        sqErr = (correct-estimation).^2;
        MSE   = mean(sqErr);
        RMSE  = sqrt(MSE);
        
        %
        varargout{1} = MSE;
        
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

else
    %%%-----------------------------------------------------------------%%%
    if strcmp(Method, 'Exact')
        % Winner-takes-all:
        [~, idx_est] = max(estimation, [], 1);
        [~, idx_cor] = max(correct, [], 1);
        
        % mean performance:
        prdiff = (idx_est==idx_cor); mPerf = mean(prdiff);
        
        % root mean squared error:
        sqErr = (correct-estimation).^2;
        MSE   = mean(sqErr);
        RMSE  = sqrt(MSE);
        
        %
        varargout{1} = MSE;
    end
end