function C = CosineProb(x, pVect1, pVect2)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes the cosyne between two probability distribution vectors
%==========================================================================

C = ((pVect1' * pVect2)/ (length(x)^2));