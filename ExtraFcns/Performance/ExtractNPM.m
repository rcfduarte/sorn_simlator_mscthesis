display('Extracting NPM...')

nContexts =     numel(unique(test_seq));


P_NPM = computeNPM(test_seq(1:size(test_seq,2)-1), state_test(:,...
    2:size(state_test,2)), nContexts);


if strcmp(Type, 'FSG')
    P_NPM = P_NPM(:,nGram-1:end);
    
    [RMSE_th_NPM, mPerf_th_NPM, KL_NPM, Div_NPM, MSE_th_NPM,...
        NNL_NPM, COS_NPM] = ComputePerformance(...
        P_NPM, target_pr, Type, 'Theoretical', o_min, test_seq);
else
    
    [RMSE_th_NPM, mPerf_th_NPM, KL_NPM, Div_NPM, ...
        MSE_th_NPM] = ComputePerformance(...
        P_NPM, test_target, Type, 'Theoretical', o_min, test_seq);
end