function [GPE, GPE2, Gscores, NGscores] = GrammaticalityClassification(seq, mark, output)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Score grammaticality classifications
%==========================================================================
outAct = sum(output);
Mk = mark(2:end);
NG_idx = find(Mk==0);

out1 = output; out1(out1<0)=0; out1 = normCols(out1);

seq(end+1) = '#';  
StrLims = strfind(seq, '#');

seqL = diff(StrLims);


%%%
% Grammatical Prediction Error:

GPE = sum(outAct(NG_idx))/sum(outAct);

%%%
% Separate the sequence into its constituent strings:
for i = 1:length(StrLims)-1
    Strings{i} = seq(StrLims(i):StrLims(i+1)-1);
    
    % determine the grammatical class of each string:
    Gclass(i) = mean(mark(StrLims(i):StrLims(i+1)-1));
    
    % account for the output of the single string:
    OutAct{i} = output(:,StrLims(i):StrLims(i+1)-2);

    %     TarAct{i} = Pr(:,StrLims(i):StrLims(i+1)-2);

    GP(i) = mean(sum(OutAct{i}));
    
    %... and for the next-symbol prob. distribution:
    OutPr{i} = out1(:,StrLims(i):StrLims(i+1)-2);
end

%%%
% Compute Likelihood scores for each string:
unSym = unique(seq);

for i = 1:length(StrLims)-1 
    
    % analyse the current string
    for n = 1:length(Strings{i})-1
        % take estimates for next symbol, starting from symbol 1 (#) to
        % the symbol before the last one (predict the last symbol):
        Probs = OutPr{i}+eps;
        % look at the current string and assess which one actually happened
        % next (seqn)
        sq = Strings{i};
        seqn = sq(n+1);
        % in the estimated probs. take the value that corresponds to the
        % actually occuring next symbol, for each symbol in the string:
        Pro(n) = Probs(strfind(unSym,seqn),n);
    end
    
    % take the product of all individual symbol predictions occurring in
    % each string; normalize by taking the log of this product and dividing
    % by the string length 
    Score(i) = -sum(log2(Pro))/(length(Strings{i})-1);
    %Sco2(i) = -log2(prod(Pro))/(length(Strings{i})-1);
end
      
% Split the scores into the ones corresponding to grammatical and
% non-grammatical strings:
Gscores = []; NGscores=[]; 

GPg = []; GPng = [];

for i = 1:length(StrLims)-1
    if Gclass(i)
        Gscores(end+1) = Score(i);
        GPng(end+1) = GP(i); 
    else
        NGscores(end+1) = Score(i);
        GPg(end+1) = GP(i);
    end
end


GPE2 = sum(GPng) / sum(GP);





% Find a suitable cut-off threshold that allows an accurate 
% differentiation of G/NG strings



% find a threshold...

% TN=[]; FN=[]; FP=[]; TP=[];
% 
% 
%  x = 0:0.0001:0.01;
% 
% for n = 1:length(x)
%     threshold = x(n);
% 
% for i=1:length(StrLims)-2
%     
%     if Score < threshold
%         Class(i) = 0;
%         if Gclass(i) == 0
%             TN(end+1) = 1;
%         else
%             FN(end+1) = 1;
%         end
%     else
%         Class(i) = 1;
%         if Gclass(i) == 1
%             TP(end+1) = 1;
%         else
%             FP(end+1) = 1;
%         end
%         
%     end
% end
% 
% FPR(n) = numel(FP) / numel(find(Gclass==0));
% TPR(n) = numel(TP) / numel(find(Gclass==1));
% 
% plot(FPR(n), TPR(n), 'or'); hold on;
% end



