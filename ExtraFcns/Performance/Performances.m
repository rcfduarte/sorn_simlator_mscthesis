%==========================================================================
% Compute Performances
%--------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(Type, 'FSG')

    [RMSE, mPerf, MSE] = ComputePerformance(Out, test_target(:, ...
        2:length(test_target)), Type, 'Exact', o_min, test_seq);

    %%% The order of the process (nGram) has to be set!! 
    
    load(['../Input/nGramModels/',sprintf('%dGram%s.mat',nGram, grammarType)])
    
    [target_pr] = ComputeTargetDist(nGram, grammarType, test_seq, Seq, Prob);

    estimation = Out(:, (nGram-1):end);
    
    [RMSE_th, mPerf_th, KL, Div, MSE_th, NNL, COS] = ComputePerformance(...
        estimation, target_pr, Type, 'Theoretical', o_min, test_seq);

    %%% Grammaticality classification
    if strcmp(StrTypeTe, 'GNG')
       
       [GPE, GPE2, Gscores, NGscores] = GrammaticalityClassification(...
           test_seq, mark, Out);
        
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'Counting')
    
    [RMSE, mPerf, MSE] = ComputePerformance(Out, test_target(:, ...
        2:length(test_target)), Type, 'Exact', o_min, test_seq);

    [RMSE_th, mPerf_th, KL, Div, MSE_th] = ComputePerformance(...
        Out, test_target, Type, 'Theoretical', o_min, test_seq);
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'Elman')
    
    [RMSE, mPerf, MSE] = ComputePerformance(Out, test_target(:, ...
        2:length(test_target)), Type, 'Exact', o_min, test_seq);
    
    [RMSE_th, mPerf_th, KL, Div, MSE_th] = ComputePerformance(...
        Out, test_target, Type, 'Theoretical', o_min, test_seq);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'AnBn') || strcmp(Type, 'AnBnCn')
    
    [RMSE, mPerf, MSE] = ComputePerformance(Out, test_target(:, ...
        2:length(test_target)), Type, 'Exact', o_min, test_seq);

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
else
    
    [RMSE, mPerf, MSE] = ComputePerformance(Out, test_target(:, ...
        2:length(test_target)), Type, 'Exact', o_min, test_seq);
end
    