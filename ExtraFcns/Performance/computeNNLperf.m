function [NNL] = computeNNLperf(seq, correct, estimation)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Computes the normalized negative log-likelihood, used to estimate
% prediction performance
%==========================================================================
T = length(correct);
x = [1:numel(unique(seq))]';


for i=1:T
    NL(i) = NormNegLogLik(x, correct(:,i)+eps, estimation(:,i)+eps);
end

NNL = - mean(NL);
