function [P_NPM] = computeNPM(seq, state, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Create a Neural Prediction Machine, i.e., clusterize the state space and
% use the clusters to compute new probabilities...
%==========================================================================
sym = unique(seq);

if nargin>2
    nClusters = varargin{1};
else
    nClusters = numel(sym);
end


% Cluster the data:
Clusters = kmeans(state', nClusters); 


for i = 1:nClusters
    
    idx = find(Clusters==i);
    
    for n = 1:length(idx)
        
        if idx(n)+1 < length(seq)
            Cause(i,n)    = seq(idx(n));
            Followers(i,n) = seq(idx(n)+1);
        end
    end
end
    

for i = 1:length(seq)
    
    for j = 1:length(sym)
        
        Count = numel(strfind(Followers(Clusters(i),:), sym(j)));
        
        P_NPM(j,i) = Count / (numel(find(Clusters==Clusters(i))));
    end
end


P_NPM = normCols(P_NPM);