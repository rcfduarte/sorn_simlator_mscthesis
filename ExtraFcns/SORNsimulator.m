if learning
    nLSteps = length(learn_seq);
    Input = [u_learn, u_train, u_test];
else
    nLSteps = 0;
    Input = [u_train, u_test];
end

nEstSteps  = length(train_seq);
nPredSteps = length(test_seq);

nSteps = nLSteps + nEstSteps + nPredSteps;

display('Simulating...')

%-------------------------------------------------------------------------
% pre-allocation
%-------------------------------------------------------------------------
stE = zeros(nE,nSteps); stI = zeros(nI,nSteps);
rDrive = zeros(nE,nSteps);
stateE = zeros(nE,nSteps); stateI = zeros(nI,nSteps);
pState = zeros(nE,nSteps);

%-------------------------------------------------------------------------
% GENERATOR LOOP
%-------------------------------------------------------------------------
for t = 1:(nSteps-1)
  %--------------------- internal state update ---------------------------
  rDrive(:,t+1) = wEE*stateE(:,t)-wEI*stateI(:,t)-tE;
  stE(:,t+1) = rDrive(:,t+1)+ W_in*Input(:,t);
  stI(:,t+1) = wIE*stateE(:,t)-tI;
  %---------------------- output state update ----------------------------
  if hstep
    stateE(:,t+1) = stE(:,t+1)>0;
    stateI(:,t+1) = stI(:,t+1)>0;
    pState(:,t+1) = rDrive(:,t+1)>0;
  elseif clpln
    stateE(:,t+1) = clplin(nE,stE(:,t+1),gain);
    stateI(:,t+1) = clplin(nI,stI(:,t+1),gain);
    pState(:,t+1) = clplin(nE,rDrive(:,t+1),gain);
  elseif sigm
    stateE(:,t+1) = sigmoid(nE,stE(:,t+1),gain);
    stateI(:,t+1) = sigmoid(nI,stI(:,t+1),gain);
    pState(:,t+1) = sigmoid(nE,rDrive(:,t+1),gain);
  end
  %--------------------- plasticity mechanisms ---------------------------
  if learning
    if t <= nLSteps
      %-------- STDP ---------
      wEE = wEE+rSTDP*wEC.* ...
        (stateE(:,t+1)*stateE(:,t)'-stateE(:,t)*stateE(:,t+1)');
      wEE(wEE<0) = 0;
      %--------- SN ----------
      wEE = normalizeRows(wEE);
      %--------- IP ----------
      tE = tE+rIP*(stateE(:,t)-hIP);
    end
  end
end

if learning
    if inputConn
        state_train = stateE(:,nLSteps+1:nLSteps+nEstSteps);
        state_test  = stateE(:, (nLSteps+nEstSteps)+1:end);
    else
         state_train = pState(:,nLSteps+1:nLSteps+nEstSteps);
         state_test  = pState(:, (nLSteps+nEstSteps)+1:end);
    end
else
    if inputConn
        state_train = stateE(:, 1:nEstSteps);
        state_test  = stateE(:, nEstSteps+1:end);

    else
        state_train = pState(:, 1:nEstSteps);
        state_test  = pState(:, nEstSteps+1:end);
    end
end
