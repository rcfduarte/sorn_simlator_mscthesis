function [sta]=clplin(n,preE,m)
for i=1:n
    if preE(i,1)<0
        sta(i,1)=0;
    elseif preE(i,1)>=0 && preE(i,1)<=1/m
        sta(i,1)=m*preE(i,1);
    elseif preE(i,1)>1/m
        sta(i,1)=1;
    end
end
end
