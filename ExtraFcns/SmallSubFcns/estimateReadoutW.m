function [wOut] = estimateReadoutW(state, teach, varargin)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

pvpmod(varargin);

%%%
wOut = (pinv(state)' * teach')';
