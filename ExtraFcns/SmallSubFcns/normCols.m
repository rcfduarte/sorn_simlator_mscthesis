function y = normCols(M)
% Normalize the columns of matrix M

for i=1:size(M,2)
    y(:,i) = M(:,i)./sum(M(:,i));
end
