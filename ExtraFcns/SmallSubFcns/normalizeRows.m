function [nW]=normalizeRows(W)
    c=size(W,2); nW=diag(1./(W*ones(c,1)))*W;
end