function [y] = sigm(x,m)

for i=1:length(x)
    y(i) = 1/(1+exp(-m*x));
end

end
