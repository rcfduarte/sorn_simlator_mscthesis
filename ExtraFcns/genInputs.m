%==========================================================================
%                       Generate input sequences
%--------------------------------------------------------------------------
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(Type, 'FSG')
    display(sprintf('Generating %d input strings, according to %s rules',...
        nStrings, grammarType));
    
    %%%#######################  LEARN  ####################################
    if learning
        display(sprintf('    - Learn Sequence: %d %s strings (~ %2f of total)', ...
            round(nStrings*learn_fraction), StrTypeTr, learn_fraction));
 
        [learn_seq] = generate_symbolicSeq(Type, round(nStrings*learn_fraction),...
            grammarType, StrTypeTr, 0);
        learn_seq(isspace(learn_seq)) = '';
    end
    
    %%%#######################  TRAIN  ####################################
    display(sprintf('    - Train Sequence: %d %s strings (~ %2f of total)', ...
        round(nStrings*train_fraction), StrTypeTr, train_fraction));
        
    %$$$%%
    if strcmp(StrTypeTr, 'GNG')      
        [train_seq, mark] = generate_symbolicSeq(Type, ...
            round(nStrings*train_fraction), grammarType, StrTypeTr, NGratio);
    else
        [train_seq] = generate_symbolicSeq(Type, ...
            round(nStrings*train_fraction), grammarType, StrTypeTr);
    end
    train_seq(isspace(train_seq)) = '';    
   
    %%%#######################  TEST  #####################################
    display(sprintf('    - Test Sequence: %d %s strings (~ %2f of total)', ...
        round(nStrings*test_fraction), ...
        StrTypeTe, test_fraction));
    
    %$$$%%
    if strcmp(StrTypeTe, 'GNG')        
        [test_seq, mark] = generate_symbolicSeq(Type, ...
            round(nStrings*test_fraction), grammarType, StrTypeTe, NGratio);
    else
        [test_seq] = generate_symbolicSeq(Type,  ...
            round(nStrings*test_fraction), grammarType, StrTypeTe);
    end
    test_seq(isspace(test_seq)) = '';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(Type, 'Elman')
    display(sprintf('Generating %d input strings (Elman Sequences)',...
        nStrings));
    
    if learning
    %%%#######################  LEARN  ####################################
        display(sprintf('    - Learn Sequence: %d strings (~ %2f of total)', ...
            round(nStrings*learn_fraction), learn_fraction));

        [learn_seq] = generate_symbolicSeq(Type, round(nStrings*learn_fraction));
    
        learn_seq(isspace(learn_seq)) = '';
    end
    
    %%%#######################  TRAIN  ####################################
    display(sprintf('    - Train Sequence: %d strings (~ %2f of total)', ...
        round(nStrings*train_fraction), train_fraction));
    
    [train_seq] = generate_symbolicSeq(Type, round(nStrings*train_fraction));
    train_seq(isspace(train_seq)) = '';    

    %%%#######################  TEST  #####################################
    display(sprintf('    - Test Sequence: %d strings (~ %2f of total)', ...
        round(nStrings*test_fraction), test_fraction));
    
    [test_seq] = generate_symbolicSeq(Type, round(nStrings*test_fraction));
    test_seq(isspace(test_seq)) = '';
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'AnBn') || strcmp(Type, 'AnBnCn')
    display(sprintf('Generating %d input strings (%s)',...
        nStrings, Type));
    
    if learning
    %%%#######################  LEARN  ####################################
        display(sprintf('    - Learn Sequence: %d strings (~ %2f of total)', ...
            round(nStrings*learn_fraction), learn_fraction));
        
         [learn_seq] = generate_symbolicSeq(Type, round(nStrings*learn_fraction), ...
            n_max);
        learn_seq(isspace(learn_seq)) = '';
    end
    
    %%%#######################  TRAIN  ####################################
    display(sprintf('    - Train Sequence: %d strings (~ %2f of total)', ...
        round(nStrings*train_fraction), train_fraction));

    [train_seq] = generate_symbolicSeq(Type, round(nStrings*train_fraction), ...
        n_max);
    train_seq(isspace(train_seq)) = ''; 
    
    %%%#######################  TEST  #####################################
    display(sprintf('    - Test Sequence: %d strings (~ %2f of total)', ...
        (round(nStrings*test_fraction)), test_fraction));
    
    [test_seq] = generate_symbolicSeq(Type, round(nStrings*test_fraction),...
        n_max);
    test_seq(isspace(test_seq)) = '';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'Counting')  
    display(sprintf('Generating %d input strings (%s)',...
        nStrings, Type));
    
    if learning
    %%%#######################  LEARN  ####################################
        display(sprintf('    - Learn Sequence: %d strings (~ %2f of total)', ...
            round(nStrings*learn_fraction), learn_fraction));

        [learn_seq] = generate_symbolicSeq(Type, round(nStrings*learn_fraction),...
            wrd_lgth);
        learn_seq(isspace(learn_seq)) = '';
    end
        
    %%%#######################  TRAIN  ####################################
    display(sprintf('    - Train Sequence: %d strings (~ %2f of total)', ...
        round(nStrings*train_fraction), train_fraction));
    
    [train_seq] = generate_symbolicSeq(Type, round(nStrings*train_fraction),...
        wrd_lgth);    
    train_seq(isspace(train_seq)) = '';
    %%%#######################  TEST  #####################################
    display(sprintf('    - Test Sequence: %d strings (~ %2f of total)', ...
        (round(nStrings*test_fraction)), test_fraction));
    
    [test_seq] = generate_symbolicSeq(Type, round(nStrings*test_fraction),...
        wrd_lgth);
    test_seq(isspace(test_seq)) = '';

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(Type, 'Laser')
    %%% Load sequences generated externally:
    uiimport; pause
    Laser = [LaserSeq{:}];
    
    if learning
        learn_seq = Laser(1:round(length(Laser)*learn_fraction));
        train_seq = Laser(round(length(Laser)*learn_fraction)+1:...
            round(length(Laser)*learn_fraction)+round(length(Laser)*...
            train_fraction));
        test_seq  = Laser(round(length(Laser)*learn_fraction)+...
            round(length(Laser)*train_fraction)+1:end);
    else
        train_seq = Laser(1:round(length(Laser)*train_fraction));
        test_seq  = Laser(round(length(Laser)*train_fraction)+1:end);
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(Type, 'Random')
    
    seq = randi(nInputUnits, [1, nStrings]);
    sym = char(97:97+(nInputUnits-1));
    
    Seq = sym(seq); 
    
    if learning
        learn_seq = Seq(1:round(length(Seq)*learn_fraction));
        train_seq = Seq(round(length(Seq)*learn_fraction)+1:...
            round(length(Seq)*learn_fraction)+...
            round(length(Seq)*train_fraction));
        test_seq = Seq(round(length(Seq)*learn_fraction) + round(...
            length(Seq)*train_fraction) +1: end);
    else
        train_seq = Seq(1:round(length(Seq)*train_fraction));
        test_seq = Seq(round(length(Seq)*train_fraction)+1:end);
    end
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Generate input and target
%==========================================================================
if inputConn
    if learning
        [learn_input, learn_target] = generate_input(learn_seq, inputConn);
    end
    
    [train_input, train_target] = generate_input(train_seq, inputConn);
    [test_input, test_target] = generate_input(test_seq, inputConn);
    
else
    if learning
        [learn_input, learn_target] = generate_input(learn_seq, inputConn,...
            nIU, nE, drive);
    end
    [train_input, train_target] = generate_input(train_seq, inputConn,...
        nIU, nE, drive);
    [test_input, test_target] = generate_input(test_seq, inputConn,...
        nIU, nE, drive);
end
