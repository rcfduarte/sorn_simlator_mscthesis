%=========================================================================
% Network connectivity
%-------------------------------------------------------------------------
display('Generating network topology...');

wEI = rand(nE,nI);                   % static weights I->E , uniform[0,1];
wEI = normalizeRows(wEI);            % normalize rows of wEI to 1;
%-------------------------------------------------------------------------
wIE = rand(nI,nE);                   % static weights E->I , uniform[0,1];
wIE = normalizeRows(wIE);            % normalize rows of wIE to 1;
%-------------------------------------------------------------------------
% Initialization of wEE
%-------------------------------------------------------------------------
wEE = zeros(nE,nE);                  % dynamic weights E->E;
ind = find(rand(nE,nE)<=lambda);     % apply wEE sparseness;
wEE(ind) = rand(length(ind),1);
wEE(eye(nE)>0) = 0;                  % self-connections = 0;
wEE = normalizeRows(wEE);            % normalize rows of wEE to 1;
wEC = wEE>0;                         % to force wEE sparsity during learning;
%-------------------------------------------------------------------------
% Initial firing thresholds
%-------------------------------------------------------------------------
tE = tEmax*rand(nE,1);               % excitatory thresholds;
tI = tImax*rand(nI,1);               % inhibitory thresholds;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Input Encodings
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if inputConn ~= 1 && inputConn ~= 0
    W_in = sprand(nE, nInputUnits, inputConn);
elseif inputConn == 0
    W_in = eye(nE,nE);
else
    W_in = rand(nE, nInputUnits);
end