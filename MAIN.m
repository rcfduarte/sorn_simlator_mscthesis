%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                ######   #######  ########  ##    ##                     %
%               ##    ## ##     ## ##     ## ###   ##                     %
%               ##       ##     ## ##     ## ####  ##                     %
%                ######  ##     ## ########  ## ## ##                     %
%                     ## ##     ## ##   ##   ##  ####                     %
%               ##    ## ##     ## ##    ##  ##   ###                     % 
%                ######   #######  ##     ## ##    ##                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                               MAIN                                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
clear all; clc; disp('Clearing workspace...');

addpath('../Input', './ExtraFcns', './ExtraFcns/SmallSubFcns', ...
    './ExtraFcns/Performance');

display('################################################################');
display('#                     SORN Simulation                          #');
display('################################################################');

%--------------------------------------------------------------------------
start = tic;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Set Parameters
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Parameters

%%%
Top = toc(start); display(sprintf('Elapsed Time: %f [sec]\n', Top));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create input data structures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
genInputs

%%%
Top3 = toc(start); display(sprintf('Elapsed Time: %f [sec] \n', Top3));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create Network
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
genTopology


%%%
display('Generating Network Topology...')
Top2 = toc(start); display(sprintf('Elapsed Time: %f [sec]\n', Top2));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Create input data structures
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
genInputs


%%%
Top3 = toc(start); display(sprintf('Elapsed Time: %f [sec] \n', Top3));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Run Simulation
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
SORNsimulator


%%%
Top4 = toc(start); display(sprintf('Elapsed Time: %f [sec] \n', Top4));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Train Readout / Estimate output
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[W_out] = estimateReadoutW(state_train(:,2:length(state_train)), ...
    train_target(:,2:length(state_train)));

Out = (W_out * state_test(:,2:end));


%%%
display('Estimating Output...')
Top5 = toc(start); display(sprintf('Elapsed Time: %f [sec] \n', Top5));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Compute Performance
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Performances


%%%
display('Computing Performance...')
Top6 = toc(start); display(sprintf('Elapsed Time: %f [sec] \n', Top6));


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Extract NPM by clustering state space
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
ExtractNPM
%    
% 
% %%%
Top7 = toc(start); display(sprintf('Elapsed Time: %f [sec] \n', Top7));

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%=========================================================================%
display('################################################################')
