%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%       MAIN
%--------------------------------------------------------------------------
addpath('./ExtraFcns', '../Input', './Analysis',...
    './ExtraFcns/Performance', './ExtraFcns/SmallSubFcns');


clear all; clc;

display('################################################################');
display('#                SORN Simulation (Parameter Sweep)             #');
display('================================================================');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

nSims = 5;

% x = [1, 5, 10, 20, 50, 100, 200, 500, 1000];

% x = [.25, .5, .75, 1, 1.25, 1.5, 1.75, 2];
%--------------------------------------------------------------------------
x = [50, 100, 200, 300, 400, 500, 600, 700, 800];

for nnn = 1:nSims
    
    
    for tt = 1:length(x);
        
        display(sprintf('Simulation %d / %d', nnn*tt, nSims*length(x)));
        
        start = tic;
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Set Parameters
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        Parameters
        
        
        %%%________________________________________________________________________
        nE = x(tt);
        display(sprintf('N = %d\n', nE));
        nI       = .2*nE;                     % number of units in the network;
    nIU = 0.05*nE;                    % Define number of symbol-specific 
hIP    = 2*nIU/nE;                    % ... target rate;
lambda = 10/(nE-1);            % the probability of a wEE-connection;

%         tImax = 1;
%         tEmax = x(tt);
%         display(sprintf('tEmax = %d', tEmax));
%         display(sprintf('tImax = %d\n', tImax))
        
        lambda = x(tt)/(nE-1);
        
        %%%________________________________________________________________________
        
        %%%
        display('Setting Parameters...')
        Top = toc(start); display(sprintf('Elapsed Time: %f [sec]\n', Top));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Create Network
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        genTopology
        
        
        %%%
        display('Generating Network Topology...')
        Top2 = toc(start); display(sprintf('Elapsed Time: %f [sec]\n', Top2));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Create input data structures
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        genInputs
        
        
        %%%
        Top3 = toc(start); display(sprintf('Elapsed Time: %f [sec] \n', Top3));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Run Simulation
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        SORNsimulator
        
        
        %%%
        Top4 = toc(start); display(sprintf('Elapsed Time: %f [sec] \n', Top4));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Train Readout / Estimate output
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [W_out] = estimateReadoutW(state_train(:,2:length(state_train)), ...
            train_target(:,2:length(state_train)));
        
        Out = W_out * state_test(:,2:end);
        
        
        %%%
        display('Estimating Output...')
        Top5 = toc(start); display(sprintf('Elapsed Time: %f [sec] \n', Top5));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Compute Performance
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        Performances
        
        
        %%%
        display('Computing Performance...')
        Top6 = toc(start); display(sprintf('Elapsed Time: %f [sec] \n', Top6));
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%% Extract NPM by clustering state space
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % ExtractNPM
        %
        %
        % %%%
        % Top7 = toc(start); display(sprintf('Elapsed Time: %f [sec] \n', Top7));
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %=========================================================================%
        display('################################################################')
        
        
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        ExactPerf(nnn, tt) = mPerf;
        exactMSE(nnn, tt) = mean(MSE);
        
        ThMSE(nnn,tt) = mean(MSE_th);
        ThPerf(nnn,tt) = mPerf_th;
%         ThNPMperf(nnn,tt) = mPerf_th_NPM;
%         MSEnpm(nnn,tt) = mean(MSE_th_NPM);
        
        if strcmp(Type, 'FSG')
            ThNNL(nnn,tt) = NNL;
%             NPM_NNL(nnn,tt) = NNL_NPM;
            
            ThCos(nnn,tt) = COS;
%             NPM_COS(nnn,tt) = COS_NPM;
            
        end
    end
end


save(sprintf('./Data/%s_SORN_%s_nE.mat', Type, grammarType), ...
    'ExactPerf', 'exactMSE', 'ThMSE', 'ThPerf', 'ThNNL', 'ThCos')