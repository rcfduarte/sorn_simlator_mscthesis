%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% SORN Set Parameters
%==========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% The following parameters can be modified:
%--- NETWORK PARAMETERS
%   - Plasticity mechanisms (on/off) (learning)
%   - Transfer Function 
%   - number of excitatory network neurons (nE) -- will modify nI and nIU
%   - maximum initial Threshold values (tEmax/tImax)
%   - learning rates (rSTDP/rIP)
%   - target firing rate (hIP)
%   - sparsity of the EE connections (lambda)
%   - sparsity of the input weights (inputConn)
%   - method used to scale the internal weights (to achieve the ESP, see
%   rescale_weights function for information)
%   - spectral radius - desired ro of W matrix...
%   - Forget steps - define the number of time steps of network activity to
%   be discarded (get rid of contamination due to initial transients)
%   - feedback and input gain - parameters to modify the strength of
%   internal and/or input connections
%   - interval from which to draw W values... 
%--- GRAMMAR PARAMETERS
%   - Grammar type - 4 ~= allowed types (see info in generate_input)
%   - Total number of strings (train+test)
%   - fraction of strings used for train
%   - Nature of the strings (should be defined when generating each of the
%   sequences (train or test) by specifying the parameter string (as
%   grammatical, non-grammatical or both (in which case the ratio of NG
%   strings should also be defined)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% NETWORK PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%-------------------------------------------------------------------------
learning = 1;                        % plasticity mechanisms on/off = 1/0;
%-------------------- transfer functions ---------------------------------
hstep = 1;                           % Heaviside step function;
clpln = 0; gain = 1;                 % clipped linear function;
sigm  = 0;                           % tanh

%-------------------------------------------------------------------------
nE       = 200; 
nI       = .2*nE;                     % number of units in the network;

%%% Input `delivery'
inputConn = 1;                        % density of W_in - if 1 full, if
                                      % set to 0, input is not weighted and 
                                      % nIU should be defined 
%%%________________________________________________________________________
% if ~inputConn          
    nIU = 0.05*nE;                    % Define number of symbol-specific 
                                      % neurons... 
    drive = 1;                        % ... and the amount of drive they 
                                      % receive
% end 
%%%________________________________________________________________________

%__________________________________________________________________________
tEmax  = 0.5; 
tImax  = 1.4;                         % maximum threshold values;
rSTDP  = .001;                        % STDP adaptation parameter;
rIP    = .001;                        % IP adaptation parameter and ...
hIP    = 2*nIU/nE;                    % ... target rate;
lambda = 10/(nE-1);            % the probability of a wEE-connection;
                               % defines the sparsity of wEE; in this case,
                               % the mean number of input (or output) wEE-
                               % connections = 10;
%__________________________________________________________________________

nForgetSteps = 0;                    % discard initial transients

o_min = 0.001;                      % minimum output threshold (to compute 
                                % output as probability dist.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% GRAMMAR / INPUT PARAMETERS
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Type = 'FSG';               % Type of task: 
                             %   - FSG
                             %   - Elman
                             %   - Counting
                             %   - AnBn
                             %   - AnBnCn
                             %   - Laser
                             %   - (...)
                             %   - Random
%==========================================================================
% Task-Specific Parameters
%==========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%  FSG  %%%%%%%%%%%%%%%%%%%%%%%
if strcmp(Type, 'FSG')
    %%%
    
    nStrings = 10000;        % total no. of strings to be generated 
                             % (train+test)
                                                    
    train_fraction = .8;     % fraction of strings for training

    grammarType = 'RG1';    % choices are: RG1/RG2/ERG/KnSq

    %%%____________________________________________________________________
    StrTypeTr = 'G';         % type of strings used in training (G/NG/GNG)
    StrTypeTe = 'G';         % type of strings used in test (G/NG/GNG)

    NGratio = 0.7;           % percentage of non-grammatical strings in the
                             % sequence (if applicable - GNG)              
    
                            
    %-----------------------------------------------------------------------                            
%     if inputConn
    %%%%%%%%%%% Set input units == nSymbols in the grammar
        if strcmp(grammarType,'RG1') || strcmp(grammarType, 'RG2')
            nInputUnits = 6;
            nGram = 3;
        elseif strcmp(grammarType,'ERG')
            nInputUnits = 8;
            nGram = 3;
        elseif strcmp(grammarType, 'KnSq')
            nInputUnits = 5;
            nGram = 5;
        end
%     end
%%%%%%%%%%%%%%%%%%%%%%%%%%  Counting  %%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'Counting')
    wrd_lgth = 8;
    nInputUnits = 6;
    nStrings = 50000;
    nStrings = round(nStrings / (wrd_lgth+2)); 
    % in order to maintain the same no. of time steps, regardless of 
    % word length
    
    train_fraction = .5;        % fraction of strings for training

%%%%%%%%%%%%%%%%%%%%%%%%%%%  AnBn  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'AnBn') || strcmp(Type, 'AnBnCn')
    n_max = 10;
    
    nStrings = 10000;           % total no. of strings to be generated 
                                % (train+test)
                                                    
    train_fraction = .8;        % fraction of strings for training
    
    nInputUnits = numel(strfind(Type, 'n'));
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%  Elman  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(Type, 'Elman')
    nStrings = 10000;           % total no. of strings to be generated 
                                % (train+test)
    nInputUnits = 6;                                                
    train_fraction = .8;        % fraction of strings for training

%%%%%%%%%%%%%%%%%%%%%%%%%%%  Laser  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'Laser')
    nStrings = 10000;           % total no. of strings to be generated 
                                % (train+test)
    nInputUnits = 4;                                                
    train_fraction = .8;        % fraction of strings for training

%%%%%%%%%%%%%%%%%%%%%%%%%%%%  Random Sequences %%%%%%%%%%%%%%%%%%%%%%%%%
elseif strcmp(Type, 'Random')
    nStrings = 10000;
    train_fraction = .8;
    nInputUnits = input('No. of symbols: ');
end                                

%%%%%%%%%----------------------------------------------------------%%%%%%%%
if learning
    nStrings = 2*nStrings;
    learn_fraction = .5;
    train_fraction = .4;        % fraction of strings for training
    test_fraction  = 1-(learn_fraction+train_fraction);
else
    train_fraction = .8;
    test_fraction  = 1-train_fraction; 
end                               
                                
