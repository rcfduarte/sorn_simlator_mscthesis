# SORN simulator
Matlab implementation of the [SORN](https://www.frontiersin.org/articles/10.3389/neuro.10.023.2009/full) model and variations. 
Work performed as part of a [master's thesis](http://hdl.handle.net/10400.1/3786) in cognitive neuroscience.


