clear all; clc;
addpath('./ExtraFcns', '../Input', './Plot');


display('################################################################');
display('#                     SORN Simulation                          #');
display('================================================================');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Parameters
%==========================================================================
%-------------------------------------------------------------------------
learning = 0;                        % plasticity mechanisms on/off = 1/0;
%-------------------- transfer functions ---------------------------------
hstep = 1;                             % Heaviside step function;
clpln = 0; gain=1;                      % clipped linear function;
sigm  = 0;                                % tanh
%=========================================================================
% Network parameters
%-------------------------------------------------------------------------
nE     = 200; 
nI     = .2*nE;                     % number of units in the network;
nIU     = .05*nE;                    % number of symbol specific input neurons;

tEmax  = 0.5; 
tImax  = 1.4;          % maximum threshold values;
rSTDP  = .001;                      % STDP adaptation parameter;
rIP    = .001;                      % IP adaptation parameter and ...
hIP    = 2*nIU/nE;                   % ... target rate;
lambda = 10/(nE-1);             % the probability of a wEE-connection;
                               % defines the sparsity of wEE; in this case,
                               % the mean number of input (or output) wEE-
                               % connections = 10;
inputConn = 1;                        % (full) input weights matrix - if 
                                      % set to 0, input is not weighted and 
                                      % nIU should be defined 
nForgetSteps = 0;                     % discard initial transients

o_min = 0.001;                  % minimum output threshold (to compute 
                                % output as probability dist.
%-------------------------------------------------------------------------

%--------------------------------------------------------------------------
%%% GRAMMAR / INPUT PARAMETERS
Type = 'FSG';               % Type of task (FSG/Elman/counting...)

if learning
    nStrings = 20000;           % total no. of strings to be generated
                                % (train+test)
    learn_fraction = .5;
    train_fraction = .4;        % fraction of strings for training
    test_fraction  = 1-(learn_fraction+train_fraction);
else
    nStrings = 10000;
    train_fraction = .8;
    test_fraction  = 1-train_fraction; 
end

%--------------------------------------------------------------------------
% Parameters Specific to the task
%==========================================================================
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
if strcmp(Type, 'FSG')
    %%%
    grammarType = 'RG1';        % choices are: RG1/RG2/ERG/KnSq

    %%%
    StrTypeTr = 'G';            % type of strings used in training (G/NG/GNG)
    StrTypeTe = 'G';            % type of strings used in test (G/NG/GNG)

    NGratio = 0.5;              % percentage of non-grammatical strings in the
                                % sequence (if applicable - GNG)                            
                            
    %--------------------------------------------------------------------------                            
    if inputConn
    %%%%%%%%%%% Set input units == nSymbols in the grammar
        if strcmp(grammarType,'RG1') || strcmp(grammarType, 'RG2')
            nInputUnits = 6;
            nGram = 3;
        elseif strcmp(grammarType,'ERG')
            nInputUnits = 8;
            nGram = 3;
        elseif strcmp(grammarType, 'KnSq')
            nInputUnits = 5;
            nGram = 5;
        end
    end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'Counting')
    wrd_lgth = 3;
    nInputUnits = 6;
    nStrings = nStrings / (wrd_lgth+2); % in order to maintain the same 
                                        % of time steps, regardless of 
                                        % word length
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
elseif strcmp(Type, 'AnBn') || strcmp(Type, 'AnBnCn')
    n_max = 10;
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end                            
%--------------------------------------------------------------------------                            
if inputConn
    if strcmp(Type, 'FSG')
        %%%%%%%%%%% Set input units == nSymbols in the grammar
        if strcmp(grammarType,'RG1') || strcmp(grammarType, 'RG2')
            nU = 6;
        elseif strcmp(grammarType,'ERG')
            nU = 8;
        elseif strcmp(grammarType, 'KnSq')
            nU = 5;
        end
    elseif strcmp(Type, 'Elman') || strcmp(Type, 'Counting')
        nU     = 6;
    elseif strcmp(Type, 'AnBn')
        nU = 2;
    elseif strcmp(Type, 'AnBnCn')
        nU = 3;
    end

end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%=========================================================================
% Network connectivity
%-------------------------------------------------------------------------
display('Generating network topology...');

wEI = rand(nE,nI);                   % static weights I->E , uniform[0,1];
wEI = normalizeRows(wEI);            % normalize rows of wEI to 1;
%-------------------------------------------------------------------------
wIE = rand(nI,nE);                   % static weights E->I , uniform[0,1];
wIE = normalizeRows(wIE);            % normalize rows of wIE to 1;
%-------------------------------------------------------------------------
% Initialization of wEE
%-------------------------------------------------------------------------
wEE = zeros(nE,nE);                  % dynamic weights E->E;
ind = find(rand(nE,nE)<=lambda);     % apply wEE sparseness;
wEE(ind) = rand(length(ind),1);
wEE(eye(nE)>0) = 0;                  % self-connections = 0;
wEE = normalizeRows(wEE);            % normalize rows of wEE to 1;
wEC = wEE>0;                         % to force wEE sparsity during learning;
%-------------------------------------------------------------------------
% Initial firing thresholds
%-------------------------------------------------------------------------
tE = tEmax*rand(nE,1);               % excitatory thresholds;
tI = tImax*rand(nI,1);               % inhibitory thresholds;
%-------------------------------------------------------------------------
%%% input weights
if inputConn ~= 1 && inputConn ~= 0
    W_in = sprand(nE, nU, inputConn);
elseif inputConn ~= 0
    W_in = rand(nE, nU);
else
    W_in = eye(nE,nU);
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
genInputs
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Simulate
%==========================================================================
if learning
    nLSteps = length(learn_seq);
    Input = [learn_input, train_input, test_input];
else
    nLSteps = 0;
    Input = [train_input, test_input];
end
    nEstSteps = length(train_seq);
    nPredSteps = length(test_seq);

nSteps = nLSteps + nEstSteps + nPredSteps;

display('Simulating...')

%-------------------------------------------------------------------------
% pre-allocation
%-------------------------------------------------------------------------
stE=zeros(nE,nSteps); stI=zeros(nI,nSteps);
rDrive=zeros(nE,nSteps);
stateE=zeros(nE,nSteps); stateI=zeros(nI,nSteps);
pState=zeros(nE,nSteps);

%-------------------------------------------------------------------------
% GENERATOR LOOP
%-------------------------------------------------------------------------
for t = 1:(nSteps-1)
  %--------------------- internal state update ---------------------------
  rDrive(:,t+1) = wEE*stateE(:,t)-wEI*stateI(:,t)-tE;
  stE(:,t+1) = rDrive(:,t+1)+ W_in*Input(:,t);
  stI(:,t+1) = wIE*stateE(:,t)-tI;
  %---------------------- output state update ----------------------------
  if hstep
    stateE(:,t+1) = stE(:,t+1)>0;
    stateI(:,t+1) = stI(:,t+1)>0;
    pState(:,t+1) = rDrive(:,t+1)>0;
  elseif clpln
    stateE(:,t+1) = clplin(nE,stE(:,t+1),gain);
    stateI(:,t+1) = clplin(nI,stI(:,t+1),gain);
    pState(:,t+1) = clplin(nE,rDrive(:,t+1),gain);
  elseif sigm
    stateE(:,t+1) = sigmoid(nE,stE(:,t+1),gain);
    stateI(:,t+1) = sigmoid(nI,stI(:,t+1),gain);
    pState(:,t+1) = sigmoid(nE,rDrive(:,t+1),gain);
  end
  %--------------------- plasticity mechanisms ---------------------------
  if learning
    if t <= nLSteps
      %-------- STDP ---------
      wEE = wEE+rSTDP*wEC.* ...
        (stateE(:,t+1)*stateE(:,t)'-stateE(:,t)*stateE(:,t+1)');
      wEE(wEE<0) = 0;
      %--------- SN ----------
      wEE = normalizeRows(wEE);
      %--------- IP ----------
      tE = tE+rIP*(stateE(:,t)-hIP);
    end
  end
end
if learning
%     state_train = pState(:,nLSteps+1:nLSteps+nEstSteps);
%     state_test  = pState(:, (nLSteps+nEstSteps)+1:end);
    
    state_train = stateE(:,nLSteps+1:nLSteps+nEstSteps);
    state_test  = stateE(:, (nLSteps+nEstSteps)+1:end);

else
    %     state_train = pState(:, 1:nEstSteps);
    %     state_test  = pState(:, nEstSteps+1:end);
    
    state_train = stateE(:, 1:nEstSteps);
    state_test  = stateE(:, nEstSteps+1:end);
end
%--------------------------------------------------------------------------
%%% Train Readout / Estimate output
[W_out] = estimateReadoutW(state_train(:,2:length(state_train)),...
    train_target(:,2:length(state_train)));

Out = tanh(W_out * state_test(:,2:length(state_test)));


Performances